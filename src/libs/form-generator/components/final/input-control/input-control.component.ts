import {Component, Injector, Input, OnInit, Signal} from '@angular/core';
import {MatFormFieldModule} from "@angular/material/form-field";
import {MatInputModule} from "@angular/material/input";
import {AbstractControl, FormControl, FormGroup, ReactiveFormsModule} from "@angular/forms";
import {AbstractFinalControlComponent} from "../../abstraction/abstract-final-control-component";
import {InputControlConfig} from "../../../models/input-control-config";
import {map, merge} from "rxjs";
import {toSignal} from "@angular/core/rxjs-interop";
import {ValidatorConfig} from "../../../models/validator-config";

@Component({
  selector: 'rt-input-control',
  templateUrl: './input-control.component.html',
  styleUrl: './input-control.component.scss',
  standalone: true,
  imports: [
    MatFormFieldModule,
    MatInputModule,
    ReactiveFormsModule
  ]
})
export class InputControlComponent<TValue, T extends {
  [p: string]: any
}> extends AbstractFinalControlComponent<T> implements OnInit {
  @Input() override group!: FormGroup<{ [K in keyof T]: AbstractControl<T[K]> }>;
  @Input() override config!: InputControlConfig;

  control!: FormControl<TValue>;
  errorSignal!: Signal<string | undefined>;

  constructor(private injector: Injector) {
    super();
  }

  ngOnInit(): void {
    this.control = <FormControl<TValue>>(this.group.get(this.config.key));

    this.errorSignal = toSignal(merge(this.control.statusChanges, this.control.valueChanges)
        .pipe(map(() => this.findFirstMatchingValidatorConfig()?.message)),
      {injector: this.injector, initialValue: this.findFirstMatchingValidatorConfig()?.message});
  }

  private findFirstMatchingValidatorConfig(): ValidatorConfig | undefined {
    return this.config.validators?.find((validator: ValidatorConfig) => this.control.errors != null && Object.keys(this.control.errors).some((key: string): boolean => validator.type === key));
  }
}

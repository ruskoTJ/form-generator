import {Component, Input, OnInit} from '@angular/core';
import {AbstractStructuralComponent} from "../../abstraction/abstract-structural-component";
import {AbstractControl, FormGroup} from '@angular/forms';
import {FormControlConfig} from "../../../models/form-control-config";
import {FinalControlInjectorDirective} from "../../../directives/final-control-injector.directive";

@Component({
  selector: 'rt-form-control',
  templateUrl: './form-control.component.html',
  styleUrl: './form-control.component.scss',
  standalone: true,
  imports: [
    FinalControlInjectorDirective
  ]
})
export class FormControlComponent<T extends {
  [p: string]: any
}> extends AbstractStructuralComponent<T> implements OnInit {
  @Input() override group!: FormGroup<{ [K in keyof T]: AbstractControl<T[K]> }>;
  @Input() override config!: FormControlConfig;

  constructor() {
    super();
  }

  ngOnInit(): void {
  }

}

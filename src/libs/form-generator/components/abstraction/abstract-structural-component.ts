import {AbstractControl, FormGroup} from "@angular/forms";
import {AbstractControlConfig} from "../../models/abstract-control-config";

export abstract class AbstractStructuralComponent<T extends {
  [p: string]: any
}> {
  group!: FormGroup<{ [K in keyof T]: AbstractControl<T[K]> }>;
  config!: AbstractControlConfig;
}

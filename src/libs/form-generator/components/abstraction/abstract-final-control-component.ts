import {AbstractControl, FormGroup} from "@angular/forms";
import {FormControlConfig} from "../../models/form-control-config";

export abstract class AbstractFinalControlComponent<T extends {
  [p: string]: any
}> {
  group!: FormGroup<{ [K in keyof T]: AbstractControl<T[K]> }>;
  config!: FormControlConfig;
}

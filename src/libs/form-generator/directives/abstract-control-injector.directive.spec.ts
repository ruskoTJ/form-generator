import {AbstractControlInjectorDirective} from './abstract-control-injector.directive';
import {ComponentFixture, TestBed} from "@angular/core/testing";

describe('AbstractControlInjectorDirective', () => {
  let fixture: ComponentFixture<AbstractControlInjectorDirective<any>>
  let component: AbstractControlInjectorDirective<any>;
  beforeEach(() => {
    fixture = TestBed.configureTestingModule({
      imports: [AbstractControlInjectorDirective],
      providers: []
    }).createComponent(AbstractControlInjectorDirective);

    fixture.detectChanges(); // initial binding

    component = fixture.componentInstance;
  });

  it('should create an instance', () => {
    expect(component).toBeDefined();
  });
});

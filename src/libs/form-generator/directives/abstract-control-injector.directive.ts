import {ComponentRef, Directive, Input, OnDestroy, OnInit, Type, ViewContainerRef} from '@angular/core';
import {AbstractControl, FormGroup} from "@angular/forms";
import {AbstractControlConfig} from "../models/abstract-control-config";
import {StructuralType} from "../models/enum/structural-type";
import {AbstractStructuralComponent} from "../components/abstraction/abstract-structural-component";
import {FormControlComponent} from "../components/structural/form-control/form-control.component";

@Directive({
  selector: '[rtAbstractControlInjector]',
  standalone: true
})
export class AbstractControlInjectorDirective<T extends {
  [p: string]: any
}> implements OnInit, OnDestroy {
  @Input({required: true}) group!: FormGroup<{ [K in keyof T]: AbstractControl<T[K]> }>;
  @Input({required: true}) config!: AbstractControlConfig;

  private structuralComponentsMapper: Map<StructuralType, Type<AbstractStructuralComponent<T>>> = new Map<StructuralType, Type<AbstractStructuralComponent<T>>>([
    [StructuralType.CONTROL, FormControlComponent<T>],
  ])

  private componentRef!: ComponentRef<AbstractStructuralComponent<T>>;

  constructor(private containerRef: ViewContainerRef) {
  }

  ngOnInit(): void {
    const component: Type<AbstractStructuralComponent<T>> = this.resolveComponentType();
    this.componentRef = this.containerRef.createComponent<AbstractStructuralComponent<T>>(component)
    this.componentRef.instance.group = this.group;
    this.componentRef.instance.config = this.config;

  }

  ngOnDestroy(): void {
    this.containerRef.clear();
    this.componentRef.destroy();
  }

  private resolveComponentType(): Type<AbstractStructuralComponent<T>> {
    const component: Type<AbstractStructuralComponent<T>> | undefined = this.structuralComponentsMapper.get(this.config.structuralType);
    if (component == undefined) {
      throw new Error(`Structural component mapper don't have any entry for key: ${this.config.structuralType}`);
    }
    return component;
  }


}

import {ComponentRef, Directive, Input, OnDestroy, OnInit, Type, ViewContainerRef} from '@angular/core';
import {AbstractControl, FormGroup} from "@angular/forms";
import {FormControlConfig} from "../models/form-control-config";
import {ControlType} from "../models/enum/control-type";
import {AbstractFinalControlComponent} from "../components/abstraction/abstract-final-control-component";
import {InputControlComponent} from "../components/final/input-control/input-control.component";

@Directive({
  selector: '[rtFinalControlInjector]',
  standalone: true
})
export class FinalControlInjectorDirective<T extends {
  [p: string]: any
}> implements OnInit, OnDestroy {
  @Input() group!: FormGroup<{ [K in keyof T]: AbstractControl<T[K]> }>;
  @Input() config!: FormControlConfig;

  private finalControlComponentMapper: Map<ControlType, Type<AbstractFinalControlComponent<T>>> = new Map<ControlType, Type<AbstractFinalControlComponent<T>>>([
    [ControlType.INPUT, InputControlComponent]
  ]);

  private componentRef!: ComponentRef<AbstractFinalControlComponent<T>>;

  constructor(private containerRef: ViewContainerRef) {
  }

  ngOnInit(): void {
    const componentType: Type<AbstractFinalControlComponent<T>> = this.resolveFinalControlComponentType();
    this.componentRef = this.containerRef.createComponent(componentType);
    this.componentRef.instance.group = this.group;
    this.componentRef.instance.config = this.config;
  }

  ngOnDestroy(): void {
    this.containerRef.clear();
    this.componentRef.destroy();
  }

  private resolveFinalControlComponentType(): Type<AbstractFinalControlComponent<T>> {
    const componentType: Type<AbstractFinalControlComponent<T>> | undefined = this.finalControlComponentMapper.get(this.config.controlType);
    if (componentType == undefined) {
      throw new Error(`final control component mapper don't have any entry for key: ${this.config.controlType}`);
    }
    return componentType;
  }
}

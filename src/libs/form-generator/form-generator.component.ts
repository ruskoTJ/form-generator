import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {FormConfig} from "./models/form-config";
import {AbstractControl, FormGroup, ReactiveFormsModule} from "@angular/forms";
import {FormUtils} from "./utils/form-utils";
import {AbstractControlInjectorDirective} from "./directives/abstract-control-injector.directive";
import {MAT_FORM_FIELD_DEFAULT_OPTIONS} from "@angular/material/form-field";
import {MatButtonModule} from "@angular/material/button";

@Component({
  selector: 'rt-form-generator',
  templateUrl: './form-generator.component.html',
  styleUrl: './form-generator.component.scss',
  standalone: true,
  imports: [
    ReactiveFormsModule,
    AbstractControlInjectorDirective,
    MatButtonModule
  ],
  providers: [
    {provide: MAT_FORM_FIELD_DEFAULT_OPTIONS, useValue: {appearance: 'outline'}}
  ]
})
export class FormGeneratorComponent<T extends { [p: string]: any }> implements OnInit {
  @Input({required: true}) config!: FormConfig;
  @Input() data!: T;

  @Output() onSubmit: EventEmitter<T> = new EventEmitter<T>();
  @Output() onButtonClick: EventEmitter<string> = new EventEmitter<string>();

  form!: FormGroup<{ [K in keyof T]: AbstractControl<T[K]> }>;

  ngOnInit(): void {
    this.form = FormUtils.initFormGroup(this.config.controls, this.data);
  }

  submitForm(): void {
    if (this.form.valid) {
      this.onSubmit.emit(<T>this.form.value);
    } else {
      this.form.markAllAsTouched();
    }
  }

  resetForm(): void {
    this.form.reset();
  }

  buttonClick(action: string): void {
    this.onButtonClick.emit(action);
  }
}

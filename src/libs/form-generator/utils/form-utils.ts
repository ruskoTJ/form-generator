import {
  AbstractControl,
  AsyncValidatorFn,
  FormControl,
  FormControlOptions,
  FormGroup,
  ValidatorFn,
  Validators
} from "@angular/forms";
import {AbstractControlConfig} from "../models/abstract-control-config";
import {StructuralType} from "../models/enum/structural-type";
import {FormControlConfig} from "../models/form-control-config";
import {ValidatorConfig} from "../models/validator-config";
import {ValidatorType} from "../models/enum/validator-type";

export class FormUtils {

  public static initFormGroup<T extends {
    [p: string]: any
  }>(abstractControlsConfig: AbstractControlConfig[], data?: T): FormGroup<{ [K in keyof T]: AbstractControl<T[K]> }> {
    const group: FormGroup = new FormGroup({});
    abstractControlsConfig.forEach((item: AbstractControlConfig): void => {
      const value = data && data.hasOwnProperty(item.key) ? data[item.key] : undefined;
      switch (item.structuralType) {
        case StructuralType.CONTROL: {
          group.addControl(item.key, this.initFormControl(<FormControlConfig>item, value));
          break;
        }
        case StructuralType.GROUP: {
          // TODO implement this
          break;
        }
        case StructuralType.ARRAY: {
          // TODO implement this
          break;
        }
        default: {
          throw new Error(`Unknown structural type: ${item.structuralType} for control with key: ${item.key}`)
        }
      }
    });
    this.carrieOnExtraProperties(group, data, abstractControlsConfig);
    return group;
  }

  private static carrieOnExtraProperties<T extends {
    [p: string]: any
  }>(group: FormGroup<{ [K in keyof T]: AbstractControl<T[K]> }>, data: T | undefined, abstractControlsConfig: AbstractControlConfig[]): void {
    if (data) {
      Object.keys(data).forEach((p: string): void => {
        if (!abstractControlsConfig.some(({key}): boolean => p === key)) {
          group.addControl(p, new FormControl(data[p], {nonNullable: true}));
        }
      });
    }
  }

  private static initFormControl<T extends {
    [p: string]: any
  }>(controlConfig: FormControlConfig, value?: T[string]): FormControl<T[string] | null | undefined> {
    let validators: ValidatorFn[] = [];
    if (controlConfig.validators && controlConfig.validators.length > 0) {
      validators = this.createValidators(controlConfig.key, controlConfig.validators);
    }
    let asyncValidators: AsyncValidatorFn[] = [];
    const opts: FormControlOptions & {
      nonNullable: true;
    } = {validators, asyncValidators, nonNullable: true}
    return new FormControl(value, opts);
  }

  private static createValidators(key: string, validatorsConfig: ValidatorConfig[]): ValidatorFn[] {
    const validators: ValidatorFn[] = [];
    validatorsConfig.forEach((item: ValidatorConfig): void => {
      validators.push(this.resolveValidator(key, item.type, item.arg));
    });
    return validators;
  }

  private static resolveValidator(key: string, type: ValidatorType, arg?: number | string | RegExp): ValidatorFn {
    switch (type) {
      case ValidatorType.REQUIRED: {
        return Validators.required;
      }
      case ValidatorType.REQUIRED_TRUE: {
        return Validators.requiredTrue;
      }
      case ValidatorType.EMAIL: {
        return Validators.email;
      }
      case ValidatorType.MIN: {
        return Validators.min(<number>arg);
      }
      case ValidatorType.MAX: {
        return Validators.max(<number>arg);
      }
      case ValidatorType.MIN_LENGTH: {
        return Validators.minLength(<number>arg);
      }
      case ValidatorType.MAX_LENGTH: {
        return Validators.maxLength(<number>arg);
      }
      case ValidatorType.PATTERN: {
        return Validators.pattern(<string | RegExp>arg);
      }
      case ValidatorType.CUSTOM: {
        // TODO implement custom validator binding
        return Validators.nullValidator
      }
      default: {
        throw new Error(`Unknown validator type ${type} for control with key ${key}`)
      }
    }
  }
}

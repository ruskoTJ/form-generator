import {ButtonConfig} from "./button-config";

export interface ButtonsConfig {
  enable: boolean;
  resetLabel?: string;
  submitLabel?: string;
  extraButtons?: ButtonConfig[]
}

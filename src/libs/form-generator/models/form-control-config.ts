import {AbstractControlConfig} from "./abstract-control-config";
import {ControlType} from "./enum/control-type";

export interface FormControlConfig extends AbstractControlConfig {
  controlType: ControlType;
}

export interface ButtonConfig {
  action: string;
  label: string;
}

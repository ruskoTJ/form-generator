import {ValidatorType} from "./enum/validator-type";

export interface ValidatorConfig {
  arg?: number | string | RegExp;
  type: ValidatorType;
  message: string;
}

import {FormControlConfig} from "./form-control-config";
import {InputType} from "./enum/input-type";

export interface InputControlConfig extends FormControlConfig {
  type: InputType;
}

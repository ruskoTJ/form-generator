export enum ValidatorType {
  REQUIRED = 'required',
  REQUIRED_TRUE = 'required',
  MIN = 'min',
  MAX = 'max',
  EMAIL = 'email',
  MIN_LENGTH = 'minlength',
  MAX_LENGTH = 'maxlength',
  PATTERN = 'pattern',
  CUSTOM = 'custom',
}

export enum StructuralType {
  CONTROL = 'CONTROL',
  GROUP = 'GROUP',
  ARRAY = 'ARRAY',
}

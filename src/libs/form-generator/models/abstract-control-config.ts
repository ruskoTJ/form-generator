import {ValidatorConfig} from "./validator-config";
import {StructuralType} from "./enum/structural-type";

export interface AbstractControlConfig {
  key: string;
  label: string;
  structuralType: StructuralType;
  validators?: ValidatorConfig[];
}

import {AbstractControlConfig} from "./abstract-control-config";
import {ButtonsConfig} from "./buttons-config";


export interface FormConfig {
  controls: AbstractControlConfig[];
  buttons: ButtonsConfig;
}

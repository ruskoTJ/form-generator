import {Component, OnInit, ViewChild} from '@angular/core';
import {FormConfig} from "../../../libs/form-generator/models/form-config";
import {FORMS_CONFIG} from "../../config/form-config";
import {FormGeneratorComponent} from "../../../libs/form-generator/form-generator.component";
import {MatCardModule} from "@angular/material/card";
import {MatButtonModule} from "@angular/material/button";

@Component({
  selector: 'rt-form-demo',
  templateUrl: './form-demo.component.html',
  styleUrl: './form-demo.component.scss',
  standalone: true,
  imports: [
    FormGeneratorComponent,
    MatCardModule,
    MatButtonModule
  ]
})
export class FormDemoComponent implements OnInit {
  @ViewChild('simpleForm') simpleForm!: FormGeneratorComponent<any>;

  emptyFormConfig: FormConfig = FORMS_CONFIG['simpleEmpty'];
  initializedFormConfig: FormConfig = FORMS_CONFIG['simpleInit']

  initializedFormValue = {
    username: 'rami',
    email: 'rami.tajouri.it@gmail.com'
  }

  ngOnInit(): void {
  }

  reset(): void {
    this.simpleForm.form.reset();
  }

  log(): void {
    console.log(this.simpleForm.form);
  }

  submit(): void {
    if (this.simpleForm.form.valid) {
      console.log(this.simpleForm.form);
    } else {
      this.simpleForm.form.markAllAsTouched();
    }
  }
}

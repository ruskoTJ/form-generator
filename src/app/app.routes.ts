import {Routes} from '@angular/router';

export const routes: Routes = [
  {
    path: '',
    loadComponent: () => import('./components/form-demo/form-demo.component').then(c => c.FormDemoComponent)
  }
];

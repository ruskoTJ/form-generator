import {FormConfig} from "../../libs/form-generator/models/form-config";
import {StructuralType} from "../../libs/form-generator/models/enum/structural-type";
import {ControlType} from "../../libs/form-generator/models/enum/control-type";
import {InputControlConfig} from "../../libs/form-generator/models/input-control-config";
import {InputType} from "../../libs/form-generator/models/enum/input-type";
import {ValidatorType} from "../../libs/form-generator/models/enum/validator-type";

export const FORMS_CONFIG: { [p: string]: FormConfig } = {
  simpleEmpty: {
    controls: [
      <InputControlConfig>{
        key: 'username',
        label: 'Username',
        structuralType: StructuralType.CONTROL,
        validators: [
          {type: ValidatorType.REQUIRED, message: 'Can\'t be empty'}
        ],
        controlType: ControlType.INPUT,
        type: InputType.TEXT,
      },
      <InputControlConfig>{
        key: 'email',
        label: 'Email',
        structuralType: StructuralType.CONTROL,
        validators: [
          {type: ValidatorType.REQUIRED, message: 'Can\'t be empty'},
          {type: ValidatorType.EMAIL, message: 'Not a valid email'}
        ],
        controlType: ControlType.INPUT,
        type: InputType.EMAIL
      }
    ],
    buttons: {
      enable: true,
      resetLabel: 'Reset',
      submitLabel: 'Confirm',
      extraButtons: [
        {
          label: 'Log',
          action: 'log'
        }
      ]
    }
  },
  simpleInit: {
    controls: [
      <InputControlConfig>{
        key: 'username',
        label: 'Username',
        structuralType: StructuralType.CONTROL,
        validators: [
          {type: ValidatorType.REQUIRED, message: 'Can\'t be empty'}
        ],
        controlType: ControlType.INPUT,
        type: InputType.TEXT,
      },
      <InputControlConfig>{
        key: 'email',
        label: 'Email',
        structuralType: StructuralType.CONTROL,
        validators: [
          {type: ValidatorType.REQUIRED, message: 'Can\'t be empty'},
          {type: ValidatorType.EMAIL, message: 'Not a valid email'}
        ],
        controlType: ControlType.INPUT,
        type: InputType.EMAIL
      }
    ],
    buttons: {
      enable: false,
      resetLabel: 'Reset',
      submitLabel: 'Confirm',
      extraButtons: [
        {
          label: 'Log',
          action: 'log'
        }
      ]
    }
  }
}
